/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      backgroundColor: theme => ({
        'theme-color': 'var(--theme-color)'
      }),
      textColor: theme => ({
        'theme-color': 'var(--theme-color)'
      }),
    },
  },
  plugins: [],
};
