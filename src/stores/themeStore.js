import { defineStore } from 'pinia';

export const useThemeStore = defineStore({
  id: 'theme',
  state: () => ({
    color: localStorage.getItem('themeColor') || '007BFF'
  }),
  actions: {
    setThemeColor(color) {
      this.color = color;
      localStorage.setItem('themeColor', color);
    }
  }
});
